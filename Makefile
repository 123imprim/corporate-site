.PHONY: deploy

deploy:
	yarn build
	ssh -i '~/.ssh/123imprim.pem' ubuntu@fak-holding.com "rm -rf /home/studiogdo/fak/*"
	scp -i '~/.ssh/123imprim.pem' -r dist/* ubuntu@fak-holding.com:/home/studiogdo/fak
	scp -i '~/.ssh/123imprim.pem' -r assets/docs ubuntu@fak-holding.com:/home/studiogdo/fak
	ssh -i '~/.ssh/123imprim.pem' ubuntu@fak-holding.com "sudo service apache2 restart"
