// @flow
import React from 'react'
import {connect} from 'react-redux'
import App from '../components/App'
import {MAX_MOBILE_WIDTH} from '../config/content'
import {updateDimensions} from '../actions'

type Props = {
    previousRoute: Object,
    nextRoute: Object,
    isMobile: boolean,
    windowWidth: number,
    updateDimensions: Function
}

type State = {}

class AppContainer extends React.Component<Props, State> {
    
    // Fire updateDimensions only on window width resize
    windowWidthChange = () => {
        if (this.props.windowWidth !== window.innerWidth) {
            this.props.updateDimensions({
                isMobile: window.innerWidth <= MAX_MOBILE_WIDTH,
                windowWidth: window.innerWidth
            })
        }
    }
    
    componentDidMount() {
        window.addEventListener("resize", this.windowWidthChange.bind(this))
    }
    
    componentWillUnmount() {
        window.removeEventListener("resize", this.windowWidthChange.bind(this))
    }
    
    render() {
        return (
            <App previousRoute={this.props.previousRoute}
                 nextRoute={this.props.nextRoute}
                 isMobile={this.props.isMobile}/>
        )
    }
}

const mapDispatchToProps = {
    updateDimensions
}

const mapStateToProps = state => ({
    previousRoute: state.route.previousRoute,
    nextRoute: state.route.nextRoute,
    isMobile: state.dimensions.isMobile,
    windowWidth: state.dimensions.windowWidth
})
export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)