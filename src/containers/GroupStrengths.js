// @flow

import React from 'react'
import {connect} from 'react-redux'
import GroupStrengths from '../components/GroupStrengths'
import type {GroupStrengthsProps} from '../components/GroupStrengths'


const GroupStrengthsContainer = (props: GroupStrengthsProps) => {
    return (
        <GroupStrengths {...props}/>
    )
}

const mapStateToProps = state => ({
    isMobile: state.dimensions.isMobile,
})

export default connect(mapStateToProps)(GroupStrengthsContainer)