// @flow

import React, {Fragment} from 'react'
import type {OfferContent} from '../config/content_type'
import '../../assets/sass/components/offer.scss'


type OfferProps = {
    offer: OfferContent,
    isExpanded: boolean,
}

type OfferState = {
    isExpanded: ?boolean,
    cardMinHeight: number
}

class Offer extends React.Component<OfferProps, OfferState> {

    // DOM elements needed for height calculation
    desc_dom: ?HTMLDivElement
    more_dom: ?HTMLDivElement

    static defaultProps = {
        isExpanded: false
    }

    constructor(props: OfferProps) {
        super(props)
        // cardMinHeight={this.state.}
        this.state = {
            isExpanded: props.isExpanded,
            cardMinHeight: 0
        }
    }

    toogleExpand() {
        if (this.desc_dom && this.more_dom) {
            const minHeight = (this.state.isExpanded) ? 0 : this.desc_dom.offsetHeight + this.more_dom.offsetHeight
            this.setState(prevState => ({
                    isExpanded: !prevState.isExpanded,
                    cardMinHeight: minHeight
                }
            ))
        }
    }

    render() {
        const offer = this.props.offer

        const testimonials =
            this.props.offer.testimonials && offer.testimonials.map((card, index) => {
                const col1 =
                    <a href={card.href} target="_blank" className="option">
                        <img className={`logo--${card.logo_format}`} src={card.logo}/>
                    </a>

                const citation = (card.cit) ? <Fragment>
                    <div className="cit">{card.cit}</div>
                    <div className="auth">{card.auth}</div>
                </Fragment> : ""
                const col2 =
                    <div className="desc">
                        <span className="title">{card.name}</span>
                        <div>{card.desc}</div>
                        {citation}
                    </div>

                const testimonial = <div key={index}>
                    <div className="testimonials__card">
                        {(index % 2 === 0) ? col2 : col1}
                        <div className="testimonials__card-sep"/>
                        {(index % 2 === 0) ? col1 : col2}
                    </div>
                </div>

                if (index === 0)
                    return testimonial
                return <Fragment key={index}>
                    <div className="testimonials__sep"/>
                    {testimonial}
                </Fragment>
            })

        const more_content = offer.more
        const complement_part = (!more_content.comp) ? "" :
            <div className="more__comp">{more_content.comp}</div>

        const testimonial_part = (testimonials.length === 0) ? "" :
            <div className="testimonials">
                <div className="testimonials__title">Quelques références...</div>
                {testimonials}
            </div>

        const more = <Fragment>
            <div className="more__content">
                <div className="img">{more_content.img}</div>
                <div className="desc">{more_content.desc}</div>
            </div>
            {complement_part}
            {testimonial_part}
        </Fragment>

        const expandedIconContent: string = this.state.isExpanded ? "-" : "+"

        const offerClassName: string = "offer" +
            (this.state.isExpanded ? " offer--expanded" : "")

        const moreClassName: string = "more" +
            (this.state.isExpanded ? " more--expanded" : "")

        const offerInner = <div className="offer__inner">
            <div className="offer__description" ref={elt => this.desc_dom = elt}>
                <div className="offer__info">
                    <div className="offer__title">{offer.title}</div>
                    <div className="offer__number-categories">
                        {this.props.offer.links}
                    </div>
                </div>
                <div className="offer__expand-icon"
                     onClick={() => this.toogleExpand()}>
                    <span className="btn">{expandedIconContent}</span>
                    <span>{offer.more_title}</span>
                </div>
                <div className={moreClassName} ref={elt => this.more_dom = elt}>
                    {more}
                </div>
            </div>
        </div>


        return <div className={offerClassName} style={{minHeight: `${this.state.cardMinHeight}px`}}>
            {offerInner}
        </div>
    }
}

export default Offer