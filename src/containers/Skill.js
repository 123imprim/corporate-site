// @flow

import React from 'react'
import type {SkillContent} from '../config/content_type'
import '../../assets/sass/components/skill.scss'
import imprimImage from '../../assets/images/123-imprim@3x.png'
import CSSTranstion from '../components/CSSTransition'
import {
    SLIDE_LEFT_TRANSITION,
    SLIDE_RIGHT_TRANSITION,
    SLIDE_TOP_TRANSITION,
    SLIDE_BOTTOM_TRANSITION
} from '../config/content'

type SkillProps = {
    skill: SkillContent,
    imgRight: boolean,
    isExpanded: ?boolean,
}

type OfferState = {
    isExpanded: ?boolean,
}


class Skill extends React.Component<SkillProps, OfferState> {
    state = {
        isExpanded: undefined,
    }

    toggleExpandOptions = (e, content) => {
        this.setState(prevState => ({
            isExpanded: content,
        }))
    }

    render() {
        const skill = this.props.skill

        // const groupWebsites = this.props.groupSites.map((site, index) => (
        //     <a href={site.href} key={index} className="groupe-site">
        //         <img className="groupe-site__image" src={site.imgSrc} alt={site.imgAlt} title={site.imgTitle}/>
        //     </a>
        // ))

        const desc = <div className="skill__description" key="desc">
            {skill.title}
        </div>

        const img = <div className="skill__main-sites" key="img">
            <a href="" className="main-site main-site--yop">
                <img src={skill.img} alt="Yop Logo" title="Yop"/>
            </a>
        </div>

        const line = (this.props.imgRight || this.props.isMobile) ? [desc, img] : [img, desc]

        const more =
            <div className={`content container ${(this.props.isExpanded) ? "" : "closed"}`}>
                <div className="content__inner">
                    <div>{this.props.skill.content}</div>
                    <div className="columns">
                        <div>{this.props.skill.col1}</div>
                        <div>{this.props.skill.col2}</div>
                    </div>
                </div>
            </div>

        return <div>
            <div className={`skill skill--${(this.props.imgRight) ? "light-blue" : "blue"}`}>
                <div className="skill__inner container">
                    {line}
                </div>
                <div className={`more ${(skill == this.props.isExpanded) ? "closed" : ""}`}
                     onClick={(e) => this.props.toogleExpandFunction(e, skill)}>En savoir plus
                </div>
            </div>
            {more}
        </div>
    }
}

export default Skill