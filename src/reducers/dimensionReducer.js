// @flow
import {MAX_MOBILE_WIDTH} from '../config/content'
import {WINDOW_RESIZE} from "../actions"

const dimensionsInitialState = {
    isMobile: window.innerWidth <= MAX_MOBILE_WIDTH,
    windowWidth: window.innerWidth
}

export type ActionType = {
    type: string,
    [string]: string,
}


export default function dimensionsReducer(state: * = dimensionsInitialState, action: ActionType) {
    switch (action.type) {
        case WINDOW_RESIZE:
            return {
                ...state,
                isMobile: window.innerWidth <= MAX_MOBILE_WIDTH,
                windowWidth: window.innerWidth
            }
        default:
            return state
    }
}