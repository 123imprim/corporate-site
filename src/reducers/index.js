// @flow

import {combineReducers} from 'redux'
import routeReducer from './routeReducer'
import dimensionsReducer from './dimensionReducer'

export default combineReducers({
    route: routeReducer,
    dimensions: dimensionsReducer,
})
