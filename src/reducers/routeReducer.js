// @flow
import {LOCATION_CHANGE} from 'react-router-redux'
import {ROUTES} from '../config/routes'

type RouteState = {
    location: ?Object,
    previousRoute: ?string,
    nextRoute: ?string,
}

const routeInitialState: RouteState = {
    location: null,
    previousRoute: null,
    nextRoute: null,
}

const getPreviousAndNextRoute = (index: number): * => {
    const numberOfPages: number = ROUTES.length
    let prevIndex: number = index - 1
    let nextIndex: number = index + 1
    // If the path doesn't exist
    if (index === -1)
        return
    // If we are in the first page
    if (index === 0) {
        prevIndex = numberOfPages - 1
    }
    // If we are in the last page
    if (index + 1 >= numberOfPages) {
        nextIndex = 0
    }
    
    return {
        prev: ROUTES[prevIndex],
        next: ROUTES[nextIndex]
    }
}

/**
 * Merge route into the global application state
 */
export default function routeReducer(state: RouteState = routeInitialState, action: any) {
    switch (action.type) {
        case LOCATION_CHANGE:
            const pathName: string = action.payload.pathname
            const routeIndex: number = ROUTES.findIndex(route => route.pathName === pathName)
            const previousAndNextRoutes = getPreviousAndNextRoute(routeIndex)
            return {
                ...state,
                location: action.payload,
                previousRoute: previousAndNextRoutes != null ? previousAndNextRoutes.prev: null,
                nextRoute: previousAndNextRoutes != null ? previousAndNextRoutes.next: null
            }
        default:
            return state
    }
}