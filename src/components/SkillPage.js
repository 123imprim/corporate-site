// @flow

import React from 'react'
import '../../assets/sass/components/skill-page.scss'
import {Animate} from './CSSTransition'
import Skill from '../containers/Skill'
import type {SkillContent} from '../config/content_type'
import {SLIDE_LEFT_TRANSITION} from '../config/content'

type SkillPageProps = {
    title: string,
    skills: Array<SkillContent>,
}

const SkillPage = (props: SkillPageProps) => {

    const skillCards = props.skills.map((skill, index) => {
        return <Skill skill={skill} imgRight={Boolean(index % 2)} key={index}/>
    })

    return (
        <div className="skill-page container">
            <h1 className="page__title">
                {props.title}
            </h1>
            <div className="skill-page__cards">
                {skillCards}
            </div>
        </div>
    )
}

export default SkillPage