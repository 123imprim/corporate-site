// @flow

import React from 'react'
import '../../assets/sass/components/header.scss'
import headerLogo from '../../assets/images/FAK.png'
import headerMobileLogo from '../../assets/images/FAK-mobile.png'

type HeaderProps = {
    isMobile: boolean
}

const Header = (props: HeaderProps) => {

    return (
        <div className="header">
            <a href='/'>
                <img src={(props.isMobile) ? headerMobileLogo : headerLogo}
                     alt="Groupe FAK logo" title="Groupe FAK logo" className="header__logo"/>
            </a>
            <div className="header__title">
                <div>
                    Le partenaire expert des enseignes de réseaux, des marques et des industriels
                </div>
            </div>
        </div>
    )
}

export default Header