// @flow

import React from 'react'
import '../../assets/sass/components/offer-page.scss'
import {Animate} from './CSSTransition'
import Offer from '../containers/Offer'
import type {OfferContent} from '../config/content_type'
import {SLIDE_LEFT_TRANSITION, SLIDE_RIGHT_TRANSITION} from '../config/content'

type OfferPageProps = {
    title: string,
    offers: Array<OfferContent>,
}

const OfferPage = (props: OfferPageProps) => {

    const offerCards = props.offers.map((offer, index) => {
        return (
            Animate(
                <Offer offer={offer} key={index}/>,
                (index % 2) ? SLIDE_LEFT_TRANSITION : SLIDE_RIGHT_TRANSITION,
                index
            )
        )
    })

    return (
        <div className="offer-page container">
            <h1 className="page__title">
                {props.title}
            </h1>
            <div className="offer-page__cards">
                {offerCards}
            </div>
        </div>
    )
}

export default OfferPage