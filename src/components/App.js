// @flow
import React from 'react'
import {Switch, Route} from 'react-router-dom'
import '../../assets/sass/styles.scss'
import Header from './Header'
import Footer from './Footer'
import SideNavigator from './SideNavigator'
import TabNavigation from './TabNavigation'
import {ROUTES} from '../config/routes.js'

type AppProps = {
    previousRoute: Object,
    nextRoute: Object,
    isMobile: boolean
}


const App = (props: AppProps) => {
    const previousSideNav = !props.isMobile &&
        <SideNavigator previous={true}
                       label={props.previousRoute.label} route={props.previousRoute.pathName}/>

    const nextSideNav = !props.isMobile &&
        <SideNavigator label={props.nextRoute.label} route={props.nextRoute.pathName}/>

    const tabNavigation = props.isMobile &&
        <TabNavigation/>

    return (
        <div className="page-container">
            <div className="section-container">
                {previousSideNav}
                <div className="section">
                    <Header isMobile={props.isMobile}/>
                    {tabNavigation}
                    <div className="section__content">
                        <Switch>
                            {ROUTES.map((item, index) =>
                                <Route key={index} exact={index === 0} path={item.pathName} component={item.component}/>)}
                        </Switch>
                    </div>
                </div>
                {nextSideNav}
            </div>
            <Footer/>
        </div>
    )
}

export default App