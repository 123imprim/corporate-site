// @flow

import React from 'react'
import '../../assets/sass/animations.scss'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import {TRANSITION_TIMEOUTS} from '../config/content'


export const Animate = (component: ?React$Element<any>,
                        transition: string,
                        index: number = -1,
                        classNameValue: string = "",
                        defaultComponent: string = "div") => {
    return CSSTranstion(component, transition, index, classNameValue, defaultComponent)

}

const CSSTranstion = (component: ?React$Element<any>,
                      transitionName: string,
                      index: number = -1,
                      classNameValue: string = "",
                      defaultComponent: string = "div") => {
    // If key is set
    const key = (index !== -1) ? {key: index} : {}
    const className = (classNameValue !== "") ? {className: classNameValue} : {}
    const transitionNames = {
        enter: transitionName + '-enter',
        enterActive: transitionName + '-enter--active',
        leave: transitionName + '-leave',
        leaveActive: transitionName + '-leave--active',
        appear: transitionName + '-appear',
        appearActive: transitionName + '-appear--active'
    }
    return (
        <ReactCSSTransitionGroup
            component={defaultComponent}
            transitionName={transitionNames}
            transitionAppear={true}
            transitionAppearTimeout={TRANSITION_TIMEOUTS[transitionName].appear}
            transitionEnterTimeout={TRANSITION_TIMEOUTS[transitionName].enter}
            transitionLeaveTimeout={TRANSITION_TIMEOUTS[transitionName].leave}
            {...key}
            {...className}
        >
            {component}
        </ReactCSSTransitionGroup>
    )
}

export default CSSTranstion