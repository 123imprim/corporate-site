// @flow
import React from 'react'

type CounterProps = {
    className : string,
    max: number,
    increment: number,
    interval: number,
}

type CounterState = {
    intervalId : number,
    count: number,
}

class Counter extends React.Component<CounterProps, CounterState> {
    static defaultProps = {
        className: '',
        increment: 1,
        interval: 30,
    }

    constructor(props: CounterProps) {
        super(props)
        this.state = {
            intervalId: 0,
            count: 0,
        }
    }

    componentDidMount() {
        const intervalId = setInterval(() => {
            this.up()
        }, this.props.interval)
        this.setState(({intervalId: intervalId, count: 0}: CounterState))
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId)
    }

    up() {
        this.setState((state, props) => {
                if (state.count >= props.max) {
                    clearInterval(state.intervalId)
                } else return {
                    count: state.count + props.increment,
                }
            }
        )
    }

    render() {
        return (
            <span className={this.props.className}>
                {this.state.count.toLocaleString('fr-FR')}
            </span>
        )
    }
}

export default Counter