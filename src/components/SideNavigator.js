// @flow

import React from 'react'
import {Link} from 'react-router-dom'
import '../../assets/sass/components/side-navigator.scss'
import nextIcon from '../../assets/images/droite.svg'
import previousIcon from '../../assets/images/gauche.svg'

type SideNavigatorProps = {
    previous: boolean,
    route: string,
    label: string
}

const SideNavigator = (props: SideNavigatorProps) => {
    const navigationIcon: string = props.previous ? previousIcon : nextIcon
    return (
        <Link to={props.route} className="side-navigator">
            <div className="side-navigator__inner">
                <div className="side-navigator__icon">
                    <img src={navigationIcon} alt="Icône de navigation" title="naviguer"/>
                </div>
                <div className="side-navigator__text">{props.label}</div>
            </div>
        </Link>
    )
}

SideNavigator.defaultProps = {
    previous: false,
}

export default SideNavigator