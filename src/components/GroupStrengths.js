// @flow

import React from 'react'
import '../../assets/sass/components/group-strengths.scss'
import Counter from './Counter'
import CSSTranstion from "./CSSTransition"
import {
    SLIDE_TOP_TRANSITION,
    SLIDE_BOTTOM_TRANSITION,
    SLIDE_RIGHT_TRANSITION,
    SLIDE_LEFT_TRANSITION
} from "../config/content"

import peopleImg from '../../assets/images/personnes.svg'
import treeImg from '../../assets/images/arbre.svg'
import caImg from '../../assets/images/euro.png'
import investmentImg from '../../assets/images/investissement.svg'
import machineImg from '../../assets/images/machines.svg'

export type GroupStrengthsProps = {
    isMobile: boolean,
    title: string,
}

const GroupStrengths = (props: GroupStrengthsProps) => {
    const caCardOrder: number = !props.isMobile ?
        4 : 3

    const treeCardOrder: number = !props.isMobile ?
        3 : 4

    const title =
        <h1 className="page__title">
            {props.title}
        </h1>

    const description = <div className="strength strength--white-bg strength--order-1">
        <div className="strength__description-wrapper strength__description-wrapper--white-bg">
            <span className="strength__description strength__description--big strength__description--white-bg">
                <div className="strength__button">SAVOIR-FAIRE</div>
                <ul>
                    <li>Production</li>
                    <li>E-procurement</li>
                    <li>Conseiller & innover</li>
                    <li>Digitalisation</li>
                    <li>Logistique & installation</li>
                </ul>
            </span>
        </div>
    </div>

    const people = CSSTranstion(
        (
            <div className="strength__inner">
                <div className="strength__image">
                    <img src={peopleImg} alt="Nombre de personnes" title="Nombre de personnes"/>
                </div>
                <div className="strength__description-wrapper">
                <span className="strength__description">
                    <div className="strength__text--bold">
                        <Counter max={100} interval={20} className="strength__text--colored"/>
                    </div>
                    collaborateurs dédiés<br/>
                    à la satisfaction client
                </span>
                </div>
            </div>
        ),
        SLIDE_TOP_TRANSITION,
        -1,
        "strength strength--dark-blue-bg"
    )

    const orders = CSSTranstion(
        (
            <div className="strength__inner">
                <div className="strength__image">
                    <img src={treeImg} alt="Arbre" title="Arbre"/>
                </div>
                <div className="strength__description-wrapper">
                <span className="strength__description">
                    <div className="strength__text--bold">+ de &nbsp;
                        <Counter max={100000} increment={1000} interval={20} className="strength__text--colored"/>
                    </div>
                    commandes<br/>
                    chaque année
                </span>
                </div>
            </div>
        ),
        !props.isMobile ? SLIDE_TOP_TRANSITION : SLIDE_LEFT_TRANSITION,
        -1,
        `strength strength--blue-bg`
    )

    const revenues = CSSTranstion(
        (
            <div className="strength__inner">
                <div className="strength__image">
                    <img src={caImg} alt="Chiffre d'Affaire" title="Chiffre d'Affaire"/>
                </div>
                <div className="strength__description-wrapper">
                <span className="strength__description">
                    <div className="strength__text--bold strength__text--colored">
                        <Counter max={20} interval={100}/> M€
                    </div>
                    de CA consolidé
                </span>
                </div>
            </div>
        ),
        !props.isMobile ? SLIDE_BOTTOM_TRANSITION : SLIDE_RIGHT_TRANSITION,
        -1,
        `strength strength--dark-blue-bg`
    )

    const investment = CSSTranstion(
        (
            <div className="strength__inner">
                <div className="strength__image strength__image--full-width">
                    <img src={investmentImg} alt="Investissement" title="Investissement"/>
                </div>
                <div className="strength__description-wrapper">
                <span className="strength__description">
                    <div className="strength__text--bold"><span
                        className="strength__text--colored">3</span> sites</div>
                    production / logistique<br/>
                    18000m<sup>2</sup> couverts
                </span>
                </div>
            </div>
        ),
        SLIDE_BOTTOM_TRANSITION,
        -1,
        "strength strength--blue-bg"
    )

    const machine = CSSTranstion(
        (
            <div className="strength__inner">
                <div className="strength__image">
                    <img src={machineImg} alt="Machines" title="Machines"/>
                </div>
                <div className="strength__description-wrapper">
                 <span className="strength__description strength__description">
                     <div className="strength__text--bold">+ de &nbsp;
                         <Counter max={500000} increment={1000} interval={1} className="strength__text--colored"/>
                     </div>
                     fichiers traités<br/>
                     sur nos serveurs sécurisés
                </span>
                </div>
            </div>
        ),
        SLIDE_BOTTOM_TRANSITION,
        -1,
        "strength strength--light-blue-bg strength--order-6"
    )

    return (
        <div className="group-strengths container">
            {title}
            <div className="group-strengths__inner">
                {description}
                {people}
                {orders}
                {revenues}
                {investment}
                {machine}
            </div>
        </div>
    )
}

export default GroupStrengths