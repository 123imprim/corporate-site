// @flow

import React from 'react'
import {ROUTES} from '../config/routes'
import {NavLink} from 'react-router-dom'
import '../../assets/sass/components/tab-navigation.scss'

type Props = {}


const TabNavigation = (props: Props) => {
    const tabs = ROUTES.map((route, index) => (
        <NavLink exact to={route.pathName} key={index} className="tab-navigation__item"
                 activeClassName="tab-navigation__item--active">{route.label}</NavLink>
    ))
    
    return (
        <div className="tab-navigation">
            {tabs}
        </div>
    )
}

export default TabNavigation