// @flow

import React from 'react'
import '../../assets/sass/components/footer.scss'
import phoneIcon from '../../assets/images/phone.svg'
import footerLogo from '../../assets/images/FAK.png'

type Props = {}

const Footer = (props: Props) => {
    return (
        <div className="footer">
            <div className="footer__inner">
                <a href="mailto:contact@groupe-fak.com"
                   className="footer__button-link">contact@groupe-fak.com</a>
                <a href="tel:04 82 53 67 50"
                   className="footer__button-link footer__button-link--with-icon footer__button-link--dark-bg footer__button-link--no-border">
                    <img src={phoneIcon} alt="icône téléphone" title="icône téléphone"/>
                    <span>04 77 49 50 00</span>
                </a>
            </div>
        </div>
    )
}

export default Footer