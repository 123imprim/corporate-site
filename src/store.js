// @flow

import {createStore, applyMiddleware, compose} from 'redux'
import {routerMiddleware} from 'react-router-redux'

import rootReducer from './reducers'

export function configureStore(initialState: Object, history: Object) {
    const routerMW = routerMiddleware(history)
    const middleWares = [
        routerMW,
    ]
    
    const enhancers = [
        applyMiddleware(...middleWares)
    ]
    
    // If Redux DevTools Extension is installed use it, otherwise use Redux compose
    const composeEnhancers =
        process.env.NODE_ENV !== 'production' &&
        typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
            window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
                shouldHotReload: false,
            }) : compose
    
    const store = createStore(
        rootReducer,
        initialState,
        composeEnhancers(...enhancers),
    )
    
    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('./reducers', () => {
            const nextRootReducer = require('./reducers/index').default
            store.replaceReducer(nextRootReducer)
        })
    }
    
    return store
}
