// @flow
import React, {Fragment} from 'react'

import type {OfferContent, SkillContent} from './content_type'
import logoImprim from '../../assets/images/logo_123imprim_215x140.png'
import logoMorassuti from '../../assets/images/logo_morassuti_215x140.png'
import logoArmony from '../../assets/images/logo_armony_215x140.png'
import logoArmonyMorassuti from '../../assets/images/logo_armony_morassuti-215x140.png'
import logoYopp from '../../assets/images/yopp@3x.png'

import yoppPresImg from '../../assets/images/yopp-pres.png'

import devImg from '../../assets/images/dev.jpg'
import conceptionImg from '../../assets/images/conception.jpg'
import signaletiqueImg from '../../assets/images/signaletique.png'
import plvImg from '../../assets/images/plv.jpg'
import projetImg from '../../assets/images/projet.jpg'

import sizeVariables from '../../assets/sass/variables/sizes.scss'

import logoAlinea from '../../assets/images/customers/logo-alinea.png'
import logoBrico from '../../assets/images/customers/logo-brico_depot.jpg'
import logoEC from '../../assets/images/customers/logo-ec.png'
import logoGMP from '../../assets/images/customers/logo-gmp.jpg'
import docImg from '../../assets/images/download_doc.png'


  export const OFFERS: Array<OfferContent> = [
    {
        title: "Approche relationnelle \"Grands Comptes\"",
        imgSrc: logoArmonyMorassuti,
        imgAlt: self.title,
        imgTitle: self.title,
        links: [
            <a href="http://www.morassuti.fr" target="_blank">MORASSUTI</a>,
            <a href="http://www.armony.online" target="_blank"> ARMONY </a>,
        ],
        more_title: "Nos sociétés",
        more: {
            img_format: "landscape",
            img: <Fragment>
                <a href="http://www.morassuti.fr" target="_blank" className="option">
                    <img src={logoMorassuti}/>
                </a>
                <a href="http://www.armony.online" target="_blank" className="option">
                    <img src={logoArmony}/>
                </a>
            </Fragment>,
            desc: <div>
                <div><strong>
                    Votre partenaire au quotidien avec une gestion personnalisée de tous vos besoins:
                </strong></div>
                <ul>
                    <li>Devis & appels d’offre,</li>
                    <li>Conseils techniques,</li>
                    <li>Échantillons & prototypes,</li>
                    <li>Suivi de fabrication,</li>
                    <li>Suivi de livraison, etc…</li>
                </ul>
                <div><strong>
                    Notre fonctionnement a été mûri pour un suivi optimal: Trinôme dédié à chaque grand compte
                </strong></div>
                <ul>
                    <li>Technico-commercial itinérant:
                        <ul>
                            <li>être au plus près pour vous accompagner dès la génèse de chacun de vos projets</li>
                        </ul>
                    </li>
                    <li>Technico-commercial sédentaire (votre contact privilégié):
                        <ul>
                            <li>c’est la garantie d’une réactivité totale à toutes vos questions</li>
                        </ul>
                    </li>
                    <li>Chargé de fabrication:
                        <ul>
                            <li>tous vos dossiers, quels qu’ils soient, sont suivis par une personne qui en connaît
                                toutes les caractéristiques
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>,
            comp: null,
        },
        testimonials: [],
    },
    {
        title: "E-commerce / Web to print",
        imgSrc: logoImprim,
        imgAlt: self.title,
        imgTitle: self.title,
        links: [
            <a href="https://123imprim.com" target="_blank">123IMPRIM</a>,
            <a href="https://123adhesifs.123imprim.com" target="_blank"> 123ADHESIFS </a>,
            <a href="https://123affiches.123imprim.com" target="_blank">123AFFICHES</a>,
            <a href="https://123baches.123imprim.com" target="_blank"> 123BACHES </a>,
            <a href="https://123panneaux.123imprim.com" target="_blank">123PANNEAUX</a>,
        ],
        more_title: "Nos sites",
        more: {
            img_format: "landscape",
            img: <a href="https://123imprim.com" target="_blank" className="option">
                <img src={logoImprim}/>
            </a>,
            desc:
                <div>
                    <ul>
                        <li>E-commerce / Web-to-print</li>
                        <li>Produits standardisés pour un achat en quelques clics</li>
                        <li>Configurateurs professionnels pour les besoin les plus complexes</li>
                        <li>Catalogue généraliste</li>
                        <li>Vérification des fichiers et processus de production automatisé</li>
                        <li>Création de fichier en ligne prêt à imprimer</li>
                    </ul>
                </div>,
            comp: "",
        },
        testimonials: [],
    },
    {
        title: "E-procurement / Digitalisation des achats",
        imgSrc: logoYopp,
        imgAlt: self.title,
        imgTitle: self.title,
        links: [
            <a href="/docs/brochure.pdf" className="doc__link" target="_blank">
                Télécharger notre brochure
                <img src={docImg}/>
            </a>

        ],
        more_title: "Nos références",
        more: {
            img_format: "landscape",
            img: <a href="https://123imprim.com" target="_blank" className="option">
                <img src={logoYopp}/>
            </a>,
            desc:
                <div>
                    <ul>
                        <li>E-commerce / Web-to-print</li>
                        <li>Produits standardisés pour un achat en quelques clics</li>
                        <li>Configurateurs professionnels pour les besoin les plus complexes</li>
                        <li>Catalogue généraliste</li>
                        <li>Vérification des fichiers et processus de production automatisé</li>
                        <li>Création de fichier en ligne prêt à imprimer (ex: vidéo pour faire un fichier de panneau
                            immobilier)
                        </li>
                    </ul>
                </div>,
            comp: <Fragment>
                LA PLATEFORME YOPP (Yown Print Plateform) PERMET:
                <ul>
                    <li>de gagner en réactivité et en efficacité</li>
                    <li>de diminuer très fortement les coûts de gestion, coûts indirects</li>
                    <li>de baisser les coûts d’achat directs (impression, infographie, logistique…)</li>
                    <li>de libérer et valoriser les équipes en les recentrant sur la création de valeur</li>
                </ul>
                <img src={yoppPresImg}/>
            </Fragment>,
        },
        testimonials: [
            {
                logo: logoAlinea,
                logo_format: "landscape",
                name: "Alinea",
                href: "#",
                desc: "Notre plate-forme d'achats YOPP, personnalisée aux besoins du réseau ALINEA permet aux magasins de gérer en autonomie leurs besoins spécifiques tout en permettant au siège de superviser. Permet également aux équipes marketing de gérer les remontées d’informations sous forme de pré-commandes pour le déploiement des campagnes nationales. Chaque magasin peut également choisir son visuel, personnaliser son visuel et accéder à tout le catalogue papeterie, affiches, PLV et signalétique défini par les services communication et marketing." +
                " (Réseau de 26 magasins de 3000 à 12000 m2)",
                cit: "",
                auth: "",
                // cit: "YOPP nous a permis de simplifier et contrôller la mise en place d'opérations commerciales",
                // auth: "Germain C. -  Coordinateur Identité Visuelle & Agencement Commercial",
            },
            {
                logo: logoGMP,
                logo_format: "square",
                name: "Grands Moulins de Paris",
                href: "#",
                desc: "Notre plate-forme digitale YOPP, personnalisée aux besoins d’animation du réseau commercial de GRANDS MOULINS DE PARIS permet aux chargés de clientèle de gérer l’ensemble des besoins de supports imprimés pour le marketing opérationnel, de la génération du fichier à la constitution du kit et à la livraison.\n" +
                " (Pour mieux servir leur plus de 8000 clients boulangers).",
                cit: "",
                auth: "",
                // cit: "Nos commerciaux ont pu de façon efficace commander des supports commerciaux et ainsi décupler leur offre à plein de boulangeries.",
                // auth: "Joris P. - Chef de projets developpement client",
            },
            {
                logo: logoEC,
                logo_format: "landscape",
                name: "Esthetic Center",
                href: "#",
                desc: "Notre plate-forme d'achats YOPP, personnalisée aux besoins du réseau Esthetic Center permet aux services centraux d’apporter une très forte plus-value à leur réseau de points de vente.  Elle comporte de nombreuses fonctionnalités pour les franchisés (qu’ils ne retrouvent pas sur les sites e-commerce web-to-print « classiques »). Les utilisateurs peuvent personnaliser leurs fichiers dans le respect de la charte nationale et d’accéder au catalogue complet de PLV et de signalétique avec une très grande réactivité." +
                " (Réseau d’environ 150 points de vente en France).",
                cit: "",
                auth: "",
            },
            {
                logo: logoBrico,
                logo_format: "square",
                name: "Bricôt Dépots",
                href: "#",
                desc: "Notre plate-forme d'achats YOPP, personnalisée aux besoins de signalétique du réseau BRICO-DEPÔT permet à l’ensemble des intervenants d’avoir une plateforme digitale collaborative. Ainsi, en quelques clics, tous les éléments de signalétique (tous formats et tous supports) peuvent être commandés. L’organisation et la gestion de la pose peuvent également être gérées via cette plateforme." +
                " (Réseau d’environ 150 points de vente en Europe).",
                cit: "",
                auth: "",
            },
        ]
    },
]

export const SKILLS: Array<SkillContent> = [
    {
        title: <Fragment>
            <div>Web-services, développement</div>
            <div>informatique et intégration</div>
        </Fragment>,
        img: devImg,
        content: "Nous disposons de tous les modules nécessaires pour construire le portail adapté à vos besoins et vos processus",
        col1:
            <ul>
                <li>Langages et logiciels open source</li>
                <li>Python, php, svg, java, react, json</li>
                <li>E-procurement et portails en mode saas</li>
                <li>Traitement automatisé des fichiers</li>
                <li>Outil de modification de fichier</li>
            </ul>,
        col2:
            <ul>
                <li>5 ingénieurs informatiques à plein temps</li>
                <li>1 enseignant chercheur</li>
                <li>Réseau d’experts</li>
            </ul>
    },
    {
        title: "Conception et bureau d'études",
        img: conceptionImg,
        content: "Chaque problématique à une solution",
        col1:
            <ul>
                <li>Graphistes et designers</li>
                <li>Mises au point graphiques</li>
                <li>Adaptation de chartes</li>
                <li>Conception d’agencement</li>
                <li>Modélisations 3D</li>
            </ul>,
        col2:
            <ul>
                <li>Nouvelles matières</li>
                <li>Nouvelles colles</li>
                <li>Nouveaux vernis</li>
                <li>Nouveaux profilés</li>
                <li>PLV carton</li>
            </ul>
    },
    {
        title: "Fabrication et installation de signalétique et décoration de points de vente",
        img: signaletiqueImg,
        content: "Nous prenons en charge la totalité de l’habillage d’un point de vente",
        col1:
            <ul>
                <li>Signalétique et décors suspendus</li>
                <li>Signalétique et décors muraux</li>
                <li>Habillage de gondole, linéaire</li>
                <li>Théâtralisation des univers</li>
                <li>Décor spécifique</li>
                <li>Signalétique générale</li>
            </ul>,
        col2:
            <ul>
                <li>Installation sur toute la France</li>
                <li>Survey, prises de cotes</li>
                <li>Montage et installation</li>
                <li>Suivi de chantier</li>
            </ul>
    },
    {
        title: "Fabrication, impression et façonnage de supports PLV, marketing et signalétique",
        img: plvImg,
        content: "Echantillon, tests d’impression, tests de montage, tests de colisage, tous types de prototypes, prototypes PLV carton",
        col1:
            <ul>
                <li>Impression numérique</li>
                <li>Impression sérigraphie</li>
                <li>Impression offset</li>
                <li>Découpe, fraisage</li>
                <li>Tous matériaux, tous formats</li>
                <li>Pliage, coudage</li>
                <li>Vernis, pelliculage</li>
            </ul>,
        col2:
            <ul>
                <li>Habillage de linéaire</li>
                <li>Théâtralisation de vitrine</li>
                <li>Marketing opérationnel</li>
                <li>Bâches et banderoles</li>
                <li>Akilux et PLV extérieures</li>
                <li>Signalétique et support rigides</li>
                <li>Coverings, adhésifs, vitrophanies</li>
            </ul>
    },
    {
        title: <Fragment>
            <div>Achats / sourcing, suivi</div>
            <div>et contrôle qualité
            </div>
        </Fragment>,
        img: projetImg,
        content:
            <div>
                <div>A prix égal nous vous apportons plus de services</div>
                <div> A qualité et service comparables, nous sommes moins chers</div>
            </div>,
        col1:
            <ul>
                <li>Analyse technique</li>
                <li>Optimisation du cahier des charges</li>
                <li>Recherche du meilleur fournisseur</li>
            </ul>
        ,
        col2:
            <ul>
                <li>Suivi de projet</li>
                <li>Contrôle qualité</li>
                <li>Stockage et picking</li>
                <li>Colisage spécifique</li>
                <li>Répartition selon réseau</li>
                <li>Suivi des expéditions</li>
            </ul>
    },
]


export const MAX_MOBILE_WIDTH: number = parseInt(sizeVariables.maxMobileWidth)

type TransitionTimeouts = {
    appear: number,
    enter: number,
    leave: number
}

export const SLIDE_LEFT_TRANSITION: string = "slide-left"
const SLIDE_LEFT_TRANSITION_TIMEOUTS: TransitionTimeouts = {
    appear: 600,
    enter: 600,
    leave: 200
}

export const SLIDE_RIGHT_TRANSITION: string = "slide-right"
const SLIDE_RIGHT_TRANSITION_TIMEOUTS: TransitionTimeouts = {
    appear: 600,
    enter: 600,
    leave: 200
}

export const SLIDE_TOP_TRANSITION: string = "slide-top"
const SLIDE_TOP_TRANSITION_TIMEOUTS: TransitionTimeouts = {
    appear: 600,
    enter: 600,
    leave: 200
}

export const SLIDE_BOTTOM_TRANSITION: string = "slide-bottom"
const SLIDE_BOTTOM_TRANSITION_TIMEOUTS: TransitionTimeouts = {
    appear: 600,
    enter: 600,
    leave: 200
}

export const TRANSITION_TIMEOUTS: {
    [transitioName: string]: {
        appear: number,
        enter: number,
        leave: number
    }
} = {}

TRANSITION_TIMEOUTS[SLIDE_LEFT_TRANSITION] = SLIDE_LEFT_TRANSITION_TIMEOUTS
TRANSITION_TIMEOUTS[SLIDE_RIGHT_TRANSITION] = SLIDE_RIGHT_TRANSITION_TIMEOUTS
TRANSITION_TIMEOUTS[SLIDE_TOP_TRANSITION] = SLIDE_TOP_TRANSITION_TIMEOUTS
TRANSITION_TIMEOUTS[SLIDE_BOTTOM_TRANSITION] = SLIDE_BOTTOM_TRANSITION_TIMEOUTS
