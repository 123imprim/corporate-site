// @flow
import React from 'react'
import type {Node} from 'react'


type moreContent = {
    img_format: string,
    img: Node,
    desc: Node,
    comp: Node,
}

type TestimonialContent = {
    logo: string,
    logo_format: string,
    name: string,
    href: string,
    desc: string,
    cit: ?string,
    auth: ?string,

}

export type OfferContent = {
    title: string,
    imgSrc: string,
    imgAlt: string,
    imgTitle: string,
    links: Array<Node>,
    more_title: string,
    more: moreContent,
    testimonials: Array<TestimonialContent>,
}

export type SkillContent = {
    title: Node,
    img: string,
    content: Node,
    col1: *,
    col2: *,
}

export type GroupWebsites = Array<{
    href: string,
    imgSrc: string,
    imgAlt: string,
    imgTitle: string,
}>

