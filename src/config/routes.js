// @flow
import React from 'react'
import type {Element} from 'react'
import {OFFERS, SKILLS} from '../config/content'
import GroupStrengths from '../containers/GroupStrengths'
import OfferPage from '../components/OfferPage'
import SkillPage from '../components/SkillPage'


const Home = () => {
    return <GroupStrengths
        title="Depuis 50 ans aux côtés des équipes communication, marketing opérationnel et signalétique."
    />
}

const Page1 = () => {
    return <OfferPage
        title="Nos offres commerciales: 3 canaux dédiés."
        offers={OFFERS}
    />
}

const Page2 = () => {
    return <SkillPage
        title="Nos axes de compétences se divisent en cinq pôles principaux."
        skills={SKILLS}
    />
}

type RouteType = Array<{
    pathName: string,
    label: string,
    component: () => Element<*>,
}>


export const ROUTES: RouteType = [
    {
        pathName: "/",
        label: "Le groupe FAK",
        component: Home,
    },
    {
        pathName: "/offre",
        label: "Nos offres commerciales",
        component: Page1,
    },
    {
        pathName: "/competences",
        label: "Nos pôles de compétences",
        component: Page2,
    },
]

