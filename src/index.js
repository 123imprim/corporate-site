// @flow

import * as React from 'react'
import {Provider} from 'react-redux'
import ReactDOM from 'react-dom'
import App from './containers/App'
import createHistory from 'history/createHashHistory'
import {ConnectedRouter} from 'react-router-redux'
import {configureStore} from './store'

const initialState: Object = {}
const history: Object = createHistory()
const store: Object = configureStore(initialState, history)
const MOUNT_NODE: ?Element = document.getElementById('app')

if (MOUNT_NODE == null) {
    throw new Error("app node not defined in DOM")
}

const render = Component => {
    ReactDOM.render(
        <Provider store={store}>
            <ConnectedRouter key={Math.random() /* Needed for hot-reload */} history={history}>
                <Component/>
            </ConnectedRouter>
        </Provider>,
        MOUNT_NODE
    )
}

render(App)

if (module.hot) {
    module.hot.accept('./containers/App', () => {
        render(App)
    })
}
