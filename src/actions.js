export const WINDOW_RESIZE: string = "RESIZED_WINDOW"

export const updateDimensions = (dimensions: Object) => (
    {
        type: WINDOW_RESIZE,
        dimensions
    }
)
