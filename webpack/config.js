module.exports = {
    SRC_DIR: 'src',
    DIST_DIR: 'dist',
    ENTRY_JS_FILE: 'index.js',
    OUTPUT_JS_FILE: 'app.bundle.js',
    INDEX_HTML_FILE: 'index.html',
}
