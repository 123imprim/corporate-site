const path = require('path')
const conf = require('./config.js')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const paths = {
    ASSETS: path.resolve(process.cwd(), 'assets'),
    DIST: path.resolve(process.cwd(), conf.DIST_DIR),
    SRC: path.resolve(process.cwd(), conf.SRC_DIR)
}

module.exports = (options) => ({
    ...options,
    output: Object.assign({
        path: paths.DIST,
        filename: conf.OUTPUT_JS_FILE
    }, options.output), // Merge with env dependent settings
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [
                    'babel-loader',
                ],
            },
            {
                test: /\.scss$/,
                use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            'css-loader',
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: true,
                                }
                            },
                            'resolve-url-loader',
                            {
                                loader: 'sass-loader',
                                options:
                                    {
                                        sourceMap: true,
                                        includePaths: [
                                            path.join(paths.ASSETS, 'scss'),
                                        ]
                                    }
                            },
                            'css-modules-flow-types-loader',
                        ]
                    }),
                )
            },
            {
                test: /\.(eot|otf|ttf|woff|woff2)$/,
                use: 'file-loader',
            },
            {
                test: /\.(jpg|png|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            limit: 10000,
                            name: 'static/media/[name].[hash:8].[ext]',
                        },
                    },
                    {
                        loader: 'image-webpack-loader',
                    },
                ]
            }
        ],
    },
    // Enable importing JS files without specifying their extensions
    //
    // So we can write:
    // import MyComponent from './my-component';
    //
    // Instead of:
    // import MyComponent from './my-component.jsx';
    resolve: {
        extensions: ['.js', '.jsx'],
    },
})
