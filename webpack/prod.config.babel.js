const path = require('path')
const conf = require('./config')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const CleanPlugin = require('clean-webpack-plugin')

// Assert this just to be safe.
// Development builds of React are slow and not intended for production.
if (process.env.NODE_ENV !== 'production') {
    throw new Error('Production builds must have NODE_ENV=production.')
}

const paths = {
    DIST: path.resolve(process.cwd(), conf.DIST_DIR),
    SRC: path.resolve(process.cwd(), conf.SRC_DIR)
}

const plugins = [
    new CleanPlugin(path.DIST),
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new ExtractTextPlugin('style.bundle.css'),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        children: true,
        minChunks: 2,
        async: true,
    }),
    new webpack.HashedModuleIdsPlugin(),
    // Minify and optimize the index.html
    new HtmlWebpackPlugin({
        template: path.join(paths.SRC, conf.INDEX_HTML_FILE),
        minify: {
            removeComments: true,
            collapseWhitespace: true,
            removeRedundantAttributes: true,
            useShortDoctype: true,
            removeEmptyAttributes: true,
            removeStyleLinkTypeAttributes: true,
            keepClosingSlash: true,
            minifyJS: true,
            minifyCSS: true,
            minifyURLs: true,
        },
        inject: true,
    }),
    new UglifyJSPlugin({
        uglifyOptions: {
            output: {
                comments: false
            },
        },
        sourceMap: false  // Change to true in case we get a bug in prod and don't want to look at minified JS
    }),
    new CompressionPlugin({
        asset: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
        threshold: 10240,
        minRatio: 0.8
    }),
]

module.exports = require('./base.config.babel')({
    entry: [
        path.join(paths.SRC, conf.ENTRY_JS_FILE),
    ],
    
    // Utilize long-term caching by adding content hashes (not compilation hashes) to compiled assets
    output: {
        filename: '[name].[chunkhash].js',
        chunkFilename: '[name].[chunkhash].chunk.js',
    },
    
    plugins: plugins,
    
    devtool: 'cheap-module-source-map',
    
    performance: {
        assetFilter: (assetFilename) => !(/(\.map$)|(^(main\.|favicon\.))/.test(assetFilename)),
    },
})
