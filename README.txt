=======================
FAK'S CORPORATE WEBSITE
=======================

TECHNOLOGIES
============

- React
- Redux
- Reselect for Redux selectors if there is a need to memoize or compose selectors
- immutability-helper for Redux updates if the state starts getting nested
- SCSS
- Webpack 3 (4 ?)
- Yarn
- Flux
- babel-env with the preset ">1% in FR"
- redux-saga for side-effects (async, etc.)


CODE STYLE
==========

- BEM for the SCSS (no nesting! :) )
- In JS, no ";" at the end of statements
- ES6+ (let, const, import, rest operator, spread operator, Symbols, etc.)
- Use Functional Stateless Components as much as possible


LAYOUT
======

Not everything will be used, but this gives room for evolution.

package.json 
.babelrc
.flowconfig
.gitignore
README.rst
webpack/

    - base.config.babel.js
    - dev.config.babel.js
    - prod.config.babel.js

src/
    - index.html
    - server.js
    - index.js
    - content.js  # Every enum and value that never changes
    - actions.js  # Redux action creators
    - selectors.js  # Redux selectors
    - content_type.js  # Types that are not used in components (these will live in the same file)
    - api.js  # For external calls
    - middleware.js  #  Redux middlewares
    - utils.js
    - reducers/
        - index.js  # Root reducer that just combines the others and exports itself
        - ExampleReducer.js
        - OtherReducer.js
    - sagas/
        - index.js  # Root saga that just combines the others and exports itself
        - example_saga.js  # Must separate functions from watchers
        - other_saga.js
    - components/  # Presentational components
        - App.js
        - ExampleComponent.js
    - containers/  # Components connected to Redux
        - ExampleContainer.js
assets/
    - images/
    - sass/
        - variables/
            - colors.scss
            - sizes.scss
        - components/
            - example_component.scss


Installation
============

npm install or yarn

- Run development environment
    npm run dev or yarn run dev

- Build a production ready bundle
    npm run build or yarn run build
